import React, { Component } from "react";
import {fetchData, postData} from "../config"


class InstitutionSignUp extends Component{
    state = {
        showDropDown: true,
        firstName:"",
        lastName:"",
        otherName:"",
        email:"",
        password:"",
        domain:"",
        phoneNumber:"",
        address: "",
        institutionName:""
    }

    handleSecurityQuestion = e => {
        this.setState({
            securityQuestionId: parseInt(e.target.value)
        })

        setTimeout(()=>{console.log(this.state.securityQuestionId)}, 2000)
    }


    handleGender = e => {
        this.setState({
            genderId: parseInt(e.target.value)
        })

        setTimeout(()=>{console.log(this.state.genderId)}, 2000)
    }

    handleInstTypeId = e => {
        this.setState({
            instTypeId: parseInt(e.target.value),
            showDropDown:false
        })

        console.log(this.state.instTypeId, "Type ID")
        setTimeout(()=>{
            fetchData(`/Institution/InstitutionTypeId?InstitutionTypeId=${this.state.instTypeId}`, data => {
                this.setState({
                  Institution: data,
                  showDropDown:true
                });
                console.log(this.state.Institution, "Institution")
               
              });

        },2000)
      
       
    }
    handleInstitutionId = e => {
        this.setState({
            InstitutionId: parseInt(e.target.value) 
        })

        const instName = document.getElementById("instName");
        const selectedInst = instName.options[instName.selectedIndex].text;
        console.log(selectedInst)

        this.setState({
            institutionName: selectedInst
        })

        setTimeout(()=>{console.log(this.state.InstitutionId, "Inst ID")}, 2000)
    }

    handleSubmit = e => {
        e.preventDefault();
        let app = this.state

        const application = {
            institutionDomain: app.domain,
            institutionContactAddress: app.address,
            institutionPhone: app.phoneNumber,
            institutionName: app.institutionName,
            ictDirectorFirstName: app.firstName,
            ictDirectorLastName: app.lastName,
            ictDirectorSurName: app.otherName,
            ictDirectorEmail: app.email,
            ictDirectorPassword: app.password,
            securityAnswer: app.securityAnswer,
            securityQuestionId: app.securityQuestionId,
            institutionId: app.InstitutionId,
            ictDirectorGenderId: app.genderId
        }

        postData("/InsitutionSignUp", application, data => {
            const { application } = this.state;
            application.push(data);
            console.log(data.response, "Response")
          });
      

    }


    componentDidMount(){
        fetchData(`/InstitutionType`, data => {
            this.setState({
              InstitutionType: data,
            });
            console.log(this.state.InstitutionType, "Institution Type")
           
          });

          fetchData(`/SecurityQuestion`, data => {
            this.setState({
              securityQuestion: data,
            });
            console.log(this.state.securityQuestion, "Security Question")
           
          });

          fetchData(`/Gender`, data => {
            this.setState({
              gender: data,
            });
            console.log(this.state.gender, "Gender")
           
          });
    }

    render(){
        return(




            <>
            <div className="container ">
            <h4 className="mt-5 text-center">Create Institution</h4>  

                <div className="card cont">
                    <h5><i>Institution Details</i></h5>
                    <hr/>
                    <form onSubmit={e => this.handleSubmit(e)}>

                <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Institution Type:</b></label>
            <div class="col-md-7">
             <select className="form-control"
             onChange={this.handleInstTypeId}
              >
               <option>--Select Institution type--</option>
               {this.state.InstitutionType && this.state.InstitutionType.map((a, i) => {
                   return (
                   <option className="form-control" value={a.id}>{a.name}</option>
                   )
               })}
              
             </select>
            </div>
          </div> 

         
            
          {this.state.showDropDown ?
            <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Institution:</b></label>
            <div class="col-md-7">
             <select id="instName" className="form-control"
             onChange={this.handleInstitutionId}
              >
               <option>--Select Institution--</option>
             {this.state.Institution && this.state.Institution.map((e, o) => {
                 return(
                 <option value={e.id}>{e.name}</option>
                 )
             })}
             </select>
            </div>
          </div> : <span className="spinner-border"></span>}

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Institution Domain:</b></label>
            <div class="col-md-7">
             <input value={this.state.domain} onChange={e => this.setState({domain: e.target.value})} type="text" className="form-control" placeholder="xyz@domain.com"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Institution Contact Address:</b></label>
            <div class="col-md-7">
             <input value={this.state.address} onChange={e => this.setState({address: e.target.value})} type="text" className="form-control" placeholder="Enter Contact Address"/>
            </div>
          </div>
          
          <br/>

            <h5><i>ICT Director Details</i></h5>
            <hr/>
          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>First Name:</b></label>
            <div class="col-md-7">
             <input value={this.state.firstName} onChange={e => this.setState({firstName: e.target.value})} type="text" className="form-control" placeholder="Enter First Name"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Last Name:</b></label>
            <div class="col-md-7">
             <input value={this.state.lastName} onChange={e => this.setState({lastName: e.target.value})} type="text" className="form-control" placeholder="Enter Last Name"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Other Name:</b></label>
            <div class="col-md-7">
             <input value={this.state.otherName} type="text" onChange={e => this.setState({otherName: e.target.value})} className="form-control" placeholder="Enter Other Name"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Phone Number:</b></label>
            <div class="col-md-7">
             <input value={this.state.phoneNumber} type="text" onChange={e => this.setState({phoneNumber: e.target.value})} className="form-control" placeholder="080XXXXXXXX"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Gender:</b></label>
            <div class="col-md-7">
             <select id="instName" className="form-control"
             onChange={this.handleGender}
              >
               <option>--Select Gender-</option>
             {this.state.gender && this.state.gender.map((e, o) => {
                 return(
                 <option value={e.id}>{e.name}</option>
                 )
             })}
             </select>
            </div>
          </div>

          

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Password:</b></label>
            <div class="col-md-7">
             <input type="password" value={this.state.password} onChange={e => this.setState({password: e.target.value})} className="form-control" placeholder="Create Password"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Confirm Password:</b></label>
            <div class="col-md-7">
             <input type="password" className="form-control" placeholder="Confirm Password"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Email:</b></label>
            <div class="col-md-7">
             <input type="text" value={this.state.email} onChange={e => this.setState({email: e.target.value})} className="form-control" placeholder="example@yahoomai.com"/>
            </div>
          </div>


          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Security Question:</b></label>
            <div class="col-md-7">
             <select className="form-control"
             onChange={this.handleSecurityQuestion}
              >
               <option>--Select Security Question--</option>
               {this.state.securityQuestion && this.state.securityQuestion.map((a, i) => {
                   return(
                   <option value={a.id}>{a.name}</option>
                   )
               })}
             </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 mt-2"><b>Security Answer:</b></label>
            <div class="col-md-7">
             <input type="text" onChange={e => this.setState({securityAnswer: e.target.value})} value={this.state.securityAnswer} className="form-control"/>
            </div>
          </div>
                    <hr/>
                 
                    <button className="btn btn-primary col-4 text">Create Institution</button>
                    </form>
                    </div>
                    
            </div>
            






    </>
            
        )
    }
}

export default InstitutionSignUp;