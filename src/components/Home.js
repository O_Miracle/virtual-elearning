import React, { Component } from "react"
import Button from "./Button"
import NotificationBox from "./NotificationBox"
import Topnav from "./Topnav"



class Home extends Component {
    state = {
        btn:false
    }

    show = () => {
        this.setState({
            btn: true
        })
    }

    render(){
        return(
            <>

                <Topnav />
                <div className="container mt-5 text-center min-vh-100">
                    <NotificationBox message="Notification Box Component"/>
                    <h2>Welcome to Olisco E-Learning Proj</h2>
                    <small>Currently building.....</small><br/><br/>

                    <Button action={this.show} title="Login"/>
                    {this.state.btn ? <h2>Button Props Active</h2> : null}

                </div>

            </>
        )
    }
}

export default Home;