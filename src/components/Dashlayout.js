import React, { Component } from 'react';  
import { Route } from 'react-router-dom';  
import SideNav from "./Sidenav";
import Adminnav from "./Adminnav";
import Header from './Header';
  
const DashboardLayout = ({children, ...rest}) => {  
    return (  
        <>
        <Header />
        <div>
            <SideNav />

            {/* Main content */}
            <div class="main-content" id="panel">
                {/*  Top Nav */}
                <Adminnav />
                
                <div className="container-fluid pt-5">
                    <div className="pt-5">
                        {children}
                    </div>
                </div>

            </div>

        </div>  
        </>
    )  
  }  
    
  const DashboardLayoutRoute = ({component: Component, ...rest}) => {  
    return (  
      <Route {...rest} render={matchProps => (  
        <DashboardLayout>  
            <Component {...matchProps} />  
        </DashboardLayout>  
      )} />  
    )  
  };  
    
  export default DashboardLayoutRoute; 