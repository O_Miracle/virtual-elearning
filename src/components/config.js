const URL = "http://97.74.6.243/onlinetutor/api";
export const fetchData = (endpoint,callback) => {

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append('Content-Type', 'application/json' );
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch(URL + endpoint , requestOptions)
    .then(response => {return response.json()})
    .then( jsondata => {
        callback(jsondata);
    })
    .catch(error => console.log("An error Ocuured: " + error))
}

export const postData = (endpoint,data,callback) => {

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append('Content-Type', 'application/json' );
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow',
      body: JSON.stringify(data)
    };

    fetch(URL + endpoint , requestOptions)
    .then(response => {return response.json()})
    .then( jsondata => {
        callback(jsondata);
    })
    .catch(error => console.log("An error Ocuured: " + error))
}

export const editData = (endpoint,data,callback) => {

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append('Content-Type', 'application/json' );
    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      redirect: 'follow',
      body: JSON.stringify(data)
    };

    fetch(URL + endpoint , requestOptions)
    .then(response => {return response.json()})
    .then( jsondata => {
        callback(jsondata);
    })
    .catch(error => console.log("An error Ocuured: " + error))
}

export const deleteData = (endpoint,callback) => {

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append('Content-Type', 'application/json' );
    var requestOptions = {
      method: 'DELETE',
      headers: myHeaders,
      redirect: 'follow',
    };

    fetch(URL + endpoint , requestOptions)
    .then(response => {return response.json()})
    .then( jsondata => {
        callback(jsondata);
    })
    .catch(error => console.log("An error Occurred: " + error))
}