import React, { Component } from "react";

function NotificationBox (props) {
    

    return(
        <>
            <div className="alert alert-success">
                {props.message}
            </div>
        </>
    )
}

export default NotificationBox;