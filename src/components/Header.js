import React, { Component } from "react"
import {Navbar, NavbarBrand, Collapse, Nav, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarToggler, NavItem, NavLink} from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css"
import logo from '../logo.svg';

import nucleo from "../assets/nucleo/css/nucleo.css"
import fontawesome from "../assets/@fortawesome/fontawesome-free/css/all.min.css"


class Header extends Component{


    render(){
        return(
            <>
            <header>
                <link rel="icon" href={logo} type="image/svg" />
                <title>e-Learn NG</title>
                <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
                />
                <link
                href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap"
                rel="stylesheet"
                />
                <link rel="stylesheet" href={nucleo} type="text/css" />
                <link rel="stylesheet" href={fontawesome} type="text/css" />
                <link
                href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css"
                rel="stylesheet"
                />
            </header>
{/* 
             <nav id="navbar-main" class="navbar navbar-horizontal navbar-main navbar-expand-lg navbar-light bg-lighter" >
             
                <div class="container">
                <a class="navbar-brand" href="pages/dashboards/dashboard.html">
                    <img src={logo} style={{height:50}}/>
                    <span className="text-sentence"> e-Learn NG</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
                    <div class="navbar-collapse-header">
                        <div class="row">
                            <div class="col-6 collapse-brand">
                            <a href="pages/dashboards/dashboard.html">
                                <img src="assets/img/brand/blue.png" />
                            </a>
                            </div>
                            <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                <span></span>
                                <span></span>
                            </button>
                            </div>
                        </div>
                        </div>
                      
                        <hr class="d-lg-none" />
                        <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                        

                        <li class="nav-item">
                            <a href="pages/examples/login.html" class="nav-link">
                            <span class="nav-link-inner--text">Login</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/examples/register.html" class="nav-link">
                            <span class="nav-link-inner--text">Register</span>
                            </a>
                        </li>

                        </ul>
                    </div>
                </div>

             </nav> */}

            </>
        )
    }
}

export default Header;