import React, { Component } from 'react';  
import { Route } from 'react-router-dom'; 
import SideNav from "./Sidenav" 
import Header from './Header';
import Topnav from './Topnav';
  
const LoginLayout = ({ children }) => (  
    <>
        <Header />
        <Topnav />
        <div>  
            {children}                                       
        </div>  
    
    </>
);  
  
  const LoginLayoutRoute = ({component: Component, ...rest}) => {  
      return (  
        <>

        <Route {...rest} render={matchProps => (  
          <LoginLayout>  
              <Component {...matchProps} />  
          </LoginLayout>  
        )} />  

        </>
    )  
  };  
  
export default LoginLayoutRoute; 