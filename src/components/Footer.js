import React from "react"
import cookie from "../assets/cookie"
import script from "../assets/script"
import $ from "jquery"

const Footer = () => (
    <div className="container-fluid bg-light" style={{backgroundColor:"#e6e6e6"}}>
        <div className="">
        </div>
        {/* Footer */}
        <footer className="footer pt-0">
            <div className="row align-items-center justify-content-center">
                <div className="col-lg-12">
                    <div className="copyright text-center text-muted">
                        © 2019 <a href="https://www.lloydant.com/" className="font-weight-bold ml-1" target="_blank">Lloydant</a>
                    </div>
                </div>
            </div>
        </footer>
        <script src={cookie}></script>
        {/* <script src={script}></script> */}
    </div>
)

export default Footer
