import React, { Component } from "react"
// import {Navbar, NavbarBrand, Collapse, Nav, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarToggler, NavItem, NavLink} from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css"
import logo from '../logo.svg';

import nucleo from "../assets/nucleo/css/nucleo.css"
import fontawesome from "../assets/@fortawesome/fontawesome-free/css/all.min.css"

import { Link } from "react-router-dom";
// reactstrap components
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col
} from "reactstrap";

class Topnav extends React.Component {
  render() {
    return (
      <>
        <Navbar className="navbar-top navbar-horizontal navbar-light bg-lighter" expand="md" >
            <Container className="px-4">
                <NavbarBrand to="/" tag={Link}>
                    <img src={logo} style={{height:50}}/>
                    <span className="text-sentence"> e-Learn NG</span>                
                </NavbarBrand>

                <button className="navbar-toggler" id="navbar-collapse-main">
                    <span className="navbar-toggler-icon" />
                </button>

                <UncontrolledCollapse navbar toggler="#navbar-collapse-main">
                    <div className="navbar-collapse-header d-md-none">
                        <Row>
                            <Col className="collapse-brand" xs="6">
                                <Link to="/">
                                    <img src={logo} style={{height:50}}/>
                                    <span className="text-sentence"> e-Learn NG</span>
                                </Link>
                            </Col>

                            <Col className="collapse-close" xs="6">
                                <button
                                className="navbar-toggler"
                                id="navbar-collapse-main"
                                >
                                <span />
                                <span />
                                </button>
                            </Col>
                        </Row>
                    </div>

                    <Nav className="ml-auto" navbar>
                        
                        <NavItem>
                            <NavLink className="nav-link-icon" to="/auth/register" tag={Link} >
                                <i className="ni ni-circle-08" />
                                <span className="nav-link-inner--text">Register</span>
                            </NavLink>
                        </NavItem>

                        <NavItem>
                            <NavLink className="nav-link-icon" to="/login" tag={Link} >
                                <i className="ni ni-key-25" />
                                <span className="nav-link-inner--text">Login</span>
                            </NavLink>
                        </NavItem>

                    </Nav>
                </UncontrolledCollapse>
            </Container>
        </Navbar>
      </>
    );
  }
}

export default Topnav;

