import React, { Component } from "react";

function Button (props) {
    

    return(
        <>
            <button className="btn btn-primary" onClick={props.action}>{props.title}</button>
        </>
    )
}

export default Button;