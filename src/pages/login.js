import React, { Component } from "react";
import {fetchData, postData} from "../components/config"

import $ from 'jquery/dist/jquery';
import "jquery/dist/jquery"

import "../assets/cookie"
import Header from "../components/Header";
import TopNav from "../components/Topnav";
// import "../assets/script"
 

class Login extends React.Component{
    state = {
        userName:"",
        password:"",
    }

    handleSubmit = event => {
        event.preventDefault();

        this.setState({ userName: event.target.username.value });
        this.setState({ password: event.target.password.value });

        console.log(this.state.username);

        const forminput = {
            username: event.target.username.value,
            password: event.target.password.value,
        }


        postData("/User", forminput, data => {
            const { forminput } = this.state;
            forminput.push(data);
            console.log(data.response, "Response")
          });

    }


  render() {

    return (
        <>

        <div>
            
            <div className="container min-vh-90">

                <div className="text-center" style={{marginTop: "10vh"}}>
                    <h1 className="text-primary">Login to your account</h1>

                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card card-custom pt-4">

                                <div className="card-body">
                                    <form method="post" onSubmit={event => { this.handleSubmit(event) }} >

                                        <div className="form-group row justify-content-center">
                                            <label for="username" className="col-md-4 font-weight-600 text-right col-form-label">Email Address</label>
                                            <div className="col-md-6">
                                                <input id="username" type="text" className="form-control" required></input>
                                            </div>
                                            
                                        </div>
                                        
                                        <div className="form-group row justify-content-center">
                                            <label for="password" className="col-md-4 font-weight-600 text-right col-form-label">Password</label>
                                            <div className="col-md-6">
                                                <input id="password" type="password" className="form-control" required></input>
                                            </div>
                                        
                                        </div>

                                        <div className="form-group row justify-content-center">
                                            <div className="col-md-4"></div>
                                            <div className="col-md-6 text-left">
                                                <button className="btn btn-primary" type="submit">Submit</button>
                                                <span className="h4 text-primary ml-3">Forgot Password?</span>
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        </>
    )

    }   

}

export default Login;