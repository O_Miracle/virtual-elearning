import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./components/Header"
import Footer from "./components/Footer"
import ReactDOM from "react-dom";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import ContactUs from "./components/ContactUs"
import History from "./components/History"

import Home from "./components/Home"
import Layout from "./components/Layout"
import Dashlayout from "./components/Dashlayout"
import Login from "./pages/login"
import LecturerProfile from "./components/pages/lecturer/profile"
import Institution from "./components/Institution/InstitutionSignUp"
import SideNav from "./components/Sidenav"
import Admin from "./components/Adminnav"

import "./assets/nucleo/css/nucleo.css";
import "./assets/@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/argon-dashboard-react.css";
import Adminnav from './components/Adminnav';

// import $ from 'jquery';
// import "jquery/dist/jquery"
// import "./assets/style.css";

// import "./assets/jquery"
// import "./assets/cookie"
// import "./assets/script"


class App extends Component {
    render(){

        return(
            <BrowserRouter>
            
            <div> 

                {/* <Header/> */}

                  <Switch>
                      <Route exact path="/" component={Home}>
                          <Redirect to="/Home" />  
                      </Route>

                      <Route path="/contactus" component={ContactUs}/>
                      <Route path="/history" component={History}/>
                      {/* <Route path="/login" component={Login}/> */}
                      <Route exact path="/home" component={Home}/>
                      <Route path="/institution" component={Institution}/>
                      <Route path="/Adminnav" component={Adminnav}/>
                      <Route path="/SideNav" component={SideNav}/>

                      <Dashlayout path="/lecturer/profile" component={LecturerProfile}/>
                      <Layout path="/login" component={Login} />  

                  </Switch>

                <Footer/>
            </div>
            
            </BrowserRouter>
        )

    }
}

export default App;
